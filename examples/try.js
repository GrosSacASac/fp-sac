import {
    logInputDecorator,
    logOutputDecorator,
    logDecorator,
    createInputDecorator,
} from "../decorators.js";
import { map, curry } from "../fp.js";


const timesTwo = x => {return x * 2;};
const multiplyPlus = (x,y,z) => {return x + y * z;};

// decorate just before
logDecorator(multiplyPlus)(4,7,9);
logDecorator(timesTwo)(21);

// decorate
const timesThreeDecorated = logDecorator(x => {return x * 3;});
timesThreeDecorated(5);

// map object
console.log(`simple map object`, map((x => {return x * 2;}), {x: 2, y: 6}));

// map array
console.log(`simple map array`, map((x => {return x * 2;}), [2, 6]));

// map Set
console.log(`simple map Set`, map((x => {return x * 2;}), new Set([2, 6])));

// map Map
console.log(`simple map Map`, map((x => {return x * 2;}), new Map([[`x`, 2], [`y`, 6]])));

const inputEnhancer = (...x) => {
    // converts to bigints
    return x.map(BigInt);
};
const makeArgumentsBigInts = createInputDecorator(inputEnhancer);

const numberLogger = makeArgumentsBigInts((a, b) => {
    console.log(a, b);
});

numberLogger(6,7);
