import { partition } from "../fp.js";


const users = [
    { 'a': `x`,  'b': 1 },
    { 'a': `y`,    'b': 2 },
    { 'a': `z`, 'b': 3 },
];
   
const [selected, dismissed] = partition(function(o) { return o.b > 2; }, users);
console.log({selected});
console.log({dismissed});
  