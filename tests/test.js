import { map, curry, firstTruethy } from "../fp.js";

{
    const result = map(function (x, y) {
        return x + y;
    }, {
        key: `value`,
        key2: `value2`,
    },
    );

    console.log(result);
}

// curry one
{
    const add = (a, b) => {
        return a + b;
    };

    const add5 = curry(add, 5);

    const result = add5(2);

    console.log(result === 7);
}


// curry multiple
{
    const add = (a, b, c) => {
        return a + b + c;
    };

    const add7 = curry(add, 5, 2);

    const result = add7(3);

    console.log(result === 10);
}


{
    const power10IfPair = (a) => {
        return a % 2 === 0 && a ** 10;
    };

    const firstPower10IfWasPair = firstTruethy(power10IfPair, [1,3,7, 2, 12]);


    console.log(firstPower10IfWasPair, firstPower10IfWasPair === 1024);
}
