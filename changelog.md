# Changelog

## 4.0.0

 * remove sequentializePromiseCreator, use decorateForceSequential from the package utilsac instead

## 3.0.0

 * rename sequentialalizePromiseCreator into sequentializePromiseCreator

## 2.2.0

 * +partition

## 2.1.0

 * +sequentializePromiseCreator


## 2.0.0

 * createInputDecorator the input decorator gets all the arguments and should return an array that maps to the original arguments

## 1.3.0

 + firstTruethy

## 1.2.0

Start a changelog, publish with license.
