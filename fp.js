export { map, curry, firstTruethy, partition };


/**
like Array.prototype.map but supports more types
works with
Object, Array, Set, Map,
*/
const map = (mapper, x) => {
    if (Array.isArray(x)) {
        return x.map(mapper);
    }
    if (x instanceof Set) {
        return new Set(Array.from(x, mapper));
    }

    const entriesMapper = ([key, value]) => {
        return [key, mapper(value, key)];
    };
    if (x instanceof Map) {
        return new Map(Array.from(x, entriesMapper));
    }
    // Object
    return Object.fromEntries(
        Object.entries(x).map(entriesMapper),
    );
};

/*array.some and array.find both stop when 1 iteration returns true
in that case .some returns true and find returns the element for which it was true
firstTruethy is the same as find, except it returns the first truethy value instead of the value */
const firstTruethy = (mapper, x) => {
    let truethyValue;
    x.some(value => {
        truethyValue = mapper(value);
        return truethyValue;
    });
    return truethyValue || undefined;
};

/*
curries 1 or more
*/
const curry = (aFunction, ...preparedArguments) => {
    return aFunction.bind(undefined, ...preparedArguments);
};

/* similar to filter but returns 2 arrays:
The one you would have with filter and another array with the rest */
const partition = (mapper, array) => {
    const inside = [];
    const out = [];
    array.forEach(element => {
        if (mapper(element)) {
            inside.push(element);
        } else {
            out.push(element);
        }
    });
    return [inside, out];
};

