export {
    logInputDecorator,
    logOutputDecorator,
    logDecorator,
    createInputDecorator,
};

// example
// const voidDecorator = (originalFunction) => {
//     return (...x) => {
//         return originalFunction(...x);
//     };
// };

const logInputDecorator = (originalFunction) => {
    return (...x) => {
        console.log(`input:`, ...x);
        return originalFunction(...x);
    };
};

const logOutputDecorator = (originalFunction) => {
    return (...x) => {
        const output = originalFunction(...x);
        console.log(`output:`, output);
        return output;
    };
};

const logDecorator = (originalFunction) => {
    return logInputDecorator(logOutputDecorator(originalFunction));
};

const createInputDecorator = (inputEnhancer) => {
    return (originalFunction) => {
        return (...x) => {
            return originalFunction(...inputEnhancer(...x));
        };
    };
};
