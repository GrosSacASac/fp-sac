# fp-sac


Read the source code. It is the documentation.

## fp.js

```js
import {map, curry, firstTruethy, partition} from "fp-sac";
```

## decorators.js

A decorator is a function that takes a function and returns a function with the same signature.

```js
import {
    logDecorator,
    sequentialalizePromiseCreator,
    createInputDecorator,
} from "fp-sac/decorators";
```
